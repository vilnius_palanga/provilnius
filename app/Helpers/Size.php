<?php

namespace App\Helpers;
use App\Image;
use App\Project;
use App\User;
use Illuminate\Support\Facades\Auth;

class Size
{
    public static function bytesToMB($size, $precision = 2, $suffix = true)
    {
        $base = log($size, 1024);
        $suffixes = array('B', 'KB', 'MB', 'GB', 'TB');
        if (!$suffix){
            return round(pow(1024, $base - floor($base)), $precision);
        }
        return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
    }

    public static function getUserTotalSpaceRaw($user){
        $projects = $user->projects;
        $itemsSize = 0;
        foreach ($projects as $project){
            foreach ($project->items as $item){
                $itemsSize = $itemsSize + (int)$item->image_size;
            }
        }
        return $itemsSize;
    }

    public static function getUserTotalSpace($user, $suffix = true){
        $itemsSize = Size::getUserTotalSpaceRaw($user);

        if(!$itemsSize){
            return 0;
        }

        if (!$suffix){
            return Size::bytesToMB($itemsSize, 2, false);
        }
        return Size::bytesToMB($itemsSize);
    }

    public static function getProjectSizeById($id){
        $project = Project::findOrFail($id);
        $itemsSize = 0;
        foreach ($project->items as $item){
            $itemsSize = $itemsSize + (int)$item->image_size;
        }
        if(!empty($itemsSize) && $itemsSize !== ''){
            return Size::bytesToMB($itemsSize);
        }
        return 0;
    }

    public static function getItemSizeById($id){
        $item = Image::findOrFail($id);
        return Size::bytesToMB($item->image_size);
    }

}