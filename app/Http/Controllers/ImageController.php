<?php

namespace App\Http\Controllers;

use App\Image;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;
use ImageOptimizer;

class ImageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api')
            ->except('show');
    }

    public function slugify($id){
        $image = Image::findOrFail($id);
        $uniqueSlug = substr(md5(microtime()), 0, 20);

        $image->slug = $uniqueSlug;
        $image->save();

        return response()
            ->json([
                'saved' => true,
                'data' => $image
            ]);
    }


    public function active($id){
        $image = Image::findOrFail($id);
        $status = $image->active ? 0 : 1;
        $image->active = $status;
        $image->save();

        return response()
            ->json([
                'saved' => true,
                'data' => $image
            ]);
    }

    public function store(Request $request)
    {
        if(!$request->hasFile('image')){
            return response()
                ->json([
                    'error' => 'no file send',
                ]);
        }

        $status = false;
        $validator = Validator::make($request->all(), [
            'image' => 'required',
            'image.*' => 'image|mimes:png,jpg,jpeg|max:5120',
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }


        $files = $request->file('image');
        if(count($files)){
            foreach ($files as $file){
                $ext = $file->extension();
                $uniqueName = substr(md5(microtime()), 0, 20);
                $path = 'projects/user_'. Auth()->id() . '/project_' . $request->project_id .'/items/';
                $imageName = $uniqueName.'.'.$ext;
                $file->storeAs('public/'.$path, $imageName);

                $image = new Image;
                $image->image = $path . $imageName;
                $image->project_id = $request->project_id;
                $image->size = $file->getClientSize();
                $image->slug = substr(md5(microtime()), 0, 20);
                $image->save();
            }
            $status = true;
        }

        return response()
            ->json([
                'saved' => $status,
            ]);
    }


    public function show($slug_or_id)
    {
        $image = Image::where('slug', $slug_or_id)->orWhere('id', $slug_or_id)->firstOrFail();
        return response()
            ->json([
                'item' => $image
            ]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'screen_type' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }

        $image = Image::findOrFail($id);
        $image->fill($request->all());
        $image->save();

        return response()
            ->json([
                'saved' => true
            ]);
    }


    public function destroy(Request $request, $id)
    {
        $image = Image::findOrFail($id);
        $image->delete();

        $file = public_path('storage/' . $image->image);
        if(file_exists($file)){
            unlink($file);
        }

        return response()
            ->json([
                'deleted' => true
            ]);
    }


    public function compress(Request $request, $id)
    {
        $image = Image::findOrFail($id);

        $file_path = public_path('storage/'.$image->image);
        ImageOptimizer::optimize($file_path);

        $sizeUpdated = filesize($file_path);
        $image->size = $sizeUpdated;
        $image->save();

        return response()
            ->json([
                'compressed' => true,
            ]);
    }

}
