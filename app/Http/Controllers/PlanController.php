<?php

namespace App\Http\Controllers;

use App\Plan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PlanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
        $user = User::where('id', Auth::id())->with('plan')->first();
        return response()
            ->json([
                'plans' => Plan::all(),
                'user' => $user
            ]);
    }

    public function update(Request $request, $id){

        $plan = Plan::findOrFail($id);
        $plan->plan_id = $request->plan_id;
        $plan->save();

        return response()
            ->json([
               'saved' => true
            ]);
    }
}
