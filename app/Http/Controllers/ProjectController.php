<?php

namespace App\Http\Controllers;

use App\Image;
use App\Project;
use App\User;
use Illuminate\Http\Request;
use Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class ProjectController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api')->except('show');
    }

    public function index()
    {
        $user = User::where('id', Auth::id())->with('plan')->first();
        $projects = Project::where('user_id', $user->id)->get();
        return response()
            ->json([
                'user' => $user,
                'projects' => $projects,
            ]);
    }

    public function slugify(Request $request, $id){
        $project = Project::findOrFail($id);
        $uniqueSlug = substr(md5(microtime()), 0, 20);

        $project->slug = $uniqueSlug;
        $project->save();

        return response()
            ->json([
                'saved' => true,
                'data' => $project,
            ]);
    }

    public function active(Request $request, $id){
        $project = Project::findOrFail($id);
        $status = $project->active ? 0 : 1;
        $project->active = $status;
        $project->save();

        return response()
            ->json([
                'saved' => true,
                'data' => $project,
            ]);
    }


    public function store(Request $request)
    {
        $request->merge(['slug'=>substr(md5(microtime()), 0, 20)]);

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:80',
            'slug' => 'unique:projects,slug,' . $request->id,
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }

        $project = new Project;
        $project->fill($request->all());
        $project->user_id = Auth::id();
        $project->save();

        return response()
            ->json([
                'saved' => true,
                'data' => $project
            ]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:80'
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }

        $project = Project::findORFail($id);
        $project->fill($request->all());
        $project->save();

        return response()
            ->json([
                'saved' => true,
                'data' => $project
            ]);
    }


    public function show($slug_or_id)
    {
        $project = Project::where('slug', $slug_or_id)->orWhere('id', $slug_or_id)->firstOrFail();
        return response()
            ->json([
                'project_items' => $project->images
            ]);
    }


    public function imagesList($id)
    {
        $project = Project::findOrFail($id);
        $images = Image::where('project_id', $id)->orderByDesc('id')->get();
        return response()
            ->json([
                'items' => $images,
                'project' => $project
            ]);
    }

    private function getProjectDir($user_id, $project_id){
        $path = 'projects'.DIRECTORY_SEPARATOR.'user_'. $user_id . DIRECTORY_SEPARATOR.'project_' . $project_id;
        return public_path('storage/' . $path);
    }

    public function destroy(Request $request, $id)
    {
        $project = Project::findOrFail($id);
        $project_dir = $this->getProjectDir( Auth::id(), $project->id );

        if(File::isDirectory( $project_dir )){
            File::deleteDirectory( $project_dir );
        }
        $project->delete();

        return response()
            ->json([
                'deleted' => true
            ]);
    }
}
