<?php

namespace App;

use Exception;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\Size;
use File;
use Illuminate\Support\Facades\URL;
use Log;

class Image extends Model
{
    protected $table = 'project_images';

    protected $fillable = [
        'name', 'slug', 'image', 'size', 'active', 'project_id', 'screen_type'
    ];

    protected $appends = [
        'size_read', 'url'
    ];

    public $timestamps = false;

    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    public function getSizeReadAttribute(){
        return Size::bytesToMB($this->size);
    }

    public function getUrlAttribute(){
        return URL::to('/pi/' . $this->slug);
    }
}
