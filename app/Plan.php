<?php

namespace App;

use App\Helpers\Size;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $table = 'user_plans';

    protected $fillable = [
        'name', 'display_name', 'price', 'file_limit'
    ];

    public $timestamps = false;

    protected $appends = ['size_readable'];

    public function user()
    {
        return $this->hasMany('App\User');
    }

    public function getSizeReadableAttribute(){
        $file_limit = (int)$this->file_limit;
        return $this->attributes['size_readable'] = Size::bytesToMB($file_limit);
    }

}
