<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Size;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Log;
use Item;

class Project extends Model
{
    protected $table = 'projects';

    protected $fillable = [
        'name', 'slug', 'active', 'user_id'
    ];

    protected $appends = ['avatar', 'images_number', 'url', 'size'];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function images()
    {
        return $this->hasMany('App\Image');
    }

    public function getAvatarAttribute(){
        $avatar = 'images/no_image.png';
        $image = DB::table('project_images')->where('project_id', $this->id)->orderBy('id', 'desc')->value("image");
        if(!empty($image)){
            $avatar = $image;
        }
        return $avatar;
    }

    public function getImagesNumberAttribute(){
        return DB::table('project_images')->where('project_id', $this->id)->get()->count();
    }

    public function getSizeAttribute(){
        $size = 0;
        $imagesSize = DB::table('project_images')->where('project_id', $this->id)->sum('size');
        if(!empty($imagesSize)){
            $size = Size::bytesToMB($imagesSize);
        }
        return $size;
    }

    public function getUrlAttribute(){
        return URL::to('/p/' .$this->slug);
    }

}
