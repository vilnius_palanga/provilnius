<?php

namespace App;

use App\Helpers\Size;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Log;
use Psy\Exception\Exception;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'username', 'plan_id'
    ];

    protected $appends = [
        'size'
    ];

    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];

    public function plan()
    {
        return $this->belongsTo('App\Plan');
    }

    public function projects()
    {
        return $this->hasMany('App\Project');
    }

    public function getSizeAttribute(){
        $size = 0;
        $userSize = DB::table('project_images')
            ->select('size')
            ->leftJoin('projects', 'projects.id', '=', 'project_images.project_id')
            ->leftJoin('users', 'users.id', '=', 'projects.user_id')
            ->where('users.id', '=', $this->id)
            ->sum('size');

        if(!empty($userSize)){
            $size = Size::bytesToMB($userSize);;
        }
        return $size;
    }

}
