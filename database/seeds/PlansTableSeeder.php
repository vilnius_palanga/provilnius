<?php

use Illuminate\Database\Seeder;
use App\Plan;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Plan::create([
            'name' => 'free',
            'display_name' => 'Free for 30 days',
            'price' => 0,
            'file_limit' => '104857600',
        ]);

        Plan::create([
            'name' => 'small',
            'display_name' => 'Small Plan',
            'price' => 3,
            'file_limit' => '104857600',
        ]);

        Plan::create([
            'name' => 'medium',
            'display_name' => 'Medium Plan',
            'price' => 6,
            'file_limit' => '209715200',
        ]);

        Plan::create([
            'name' => 'large',
            'display_name' => 'Large Plan',
            'price' => 9,
            'file_limit' => '314572800',
        ]);
    }
}
