<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'username' => 'admin',
            'password' => bcrypt('mc5555mc'),
            'email' => 'edgaras@protonmail.com',
            'plan_id' => null,
            'activated' => 1
        ]);
    }
}
