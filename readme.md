# Single Page Application

### Frameworks and Libraries:

- Laravel 5.4
- Vue.js 2.2
- Vue Router
- Axios

### Installation

`git clone`
`composer install`
`npm install`

### Laravel setup
`Setup configs in .env (copy file from .env.example)`

### Laravel commands
`php artisan migrate --seed`

`php artisan key:generate`

`php artisan storage:link`
