import Vue from 'vue';
import App from './App.vue';
import router from './router/index';

window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');
import Dropzone from 'vue2-dropzone'

const app = new Vue({
	el: '#root',
	template: `<app></app>`,
	components: { App },
	router
});