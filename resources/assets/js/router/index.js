import Vue from 'vue';
import VueRouter from 'vue-router';

import Login from '../views/Auth/Login.vue';
import Register from '../views/Auth/Register.vue';
import NotFound from '../views/NotFound.vue';

import Dashboard from '../views/Dashboard/Index.vue';
import Plans from '../views/Dashboard/Plans.vue';
import User from '../views/Dashboard/User.vue';
import ProjectItems from '../views/Project/Items.vue';
import ProjectShow from '../views/Project/Show.vue';
import ItemShow from '../views/Item/Show.vue';

Vue.use(VueRouter);

const router = new VueRouter({
	mode: 'history',
	routes: [
		{ path: '/register', component: Register },
        { path: '/login', component: Login },
        { path: '/', component: Dashboard },

        { path: '/not-found', component: NotFound },
        { path: '*', component: NotFound },

        { path: '/dashboard', component: Dashboard },
        { path: '/plans', component: Plans },
        { path: '/user', component: User },
        { path: '/project/:id/items', component: ProjectItems },

        { path: '/p/:slug', component: ProjectShow, props: {public_link: true} },
        { path: '/pi/:slug', component: ItemShow, props: {public_link: true} },
    ]
});

export default router
