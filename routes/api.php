<?php

Route::prefix('v1')->group(function () {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('register', 'AuthController@register');

    Route::resource('project', 'ProjectController');
    Route::get('project/{id}/items', 'ProjectController@imagesList');
    Route::post('project/{id}/active', 'ProjectController@active');
    Route::post('project/{id}/slugify', 'ProjectController@slugify');
    Route::get('/p/{slug}', 'ProjectController@show');


    Route::resource('item', 'ImageController');
    Route::post('item/{id}/active', 'ImageController@active');
    Route::post('item/{id}/compress', 'ImageController@compress');
    Route::post('item/{id}/slugify', 'ImageController@slugify');
    Route::get('/pi/{slug}', 'ImageController@show');

    Route::resource('plan', 'PlanController');
    Route::resource('user', 'UserController');
    Route::post('user/{id}/changePlan', 'UserController@changePlan');

});

